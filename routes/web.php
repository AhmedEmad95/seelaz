<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Auth::routes();*/

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/language','HomeController@Changelang');

Route::group(['prefix'=>'admin','middleware'=>'admin'],function () {

Route::get('/', 'HomeController@index')->name('home');

// Advertisment  route
    Route::resource('/adv', 'admin\AdvertismentController');

  // category route
    Route::resource('/category', 'Admin\CategoryController');

    //Blog   route
    Route::resource('/blog', 'admin\BlogController');

     //testomanial   route
    Route::resource('/testomanial', 'admin\TestimonialController');

         //account route
    Route::resource('/account', 'admin\Acounts_controller');

     Route::resource('permission', 'PermissionController');
       Route::resource('role', 'RoleController');  
      



    // Product  route
    Route::resource('/product', 'admin\ProductController');
      Route::get('/product1', 'admin\ProductController@index1');
    Route::get('/pindingproduct/{id}', 'admin\ProductController@pindingproduct')->name('pindingproduct');
    Route::get('/activeproduct/{id}', 'admin\ProductController@activeproduct')->name('activeproduct');


//offer route
 Route::resource('/offer', 'admin\OfferController');

//setting
     //Route::resource('/seetting', 'admin\SeettingController');
    Route::resource('/about', 'admin\AboutController');


    //notification
     Route::get('/shownotification', 'admin\NotificationController@shownotification')->name('shownotification');
    Route::post('/sendnotification', 'admin\NotificationController@sendnotification')->name('sendnotification');
     

});











$this->get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('admin/login', 'Auth\LoginController@login');
$this->get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');