<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dento || @yield('title')  </title>

    <!-- responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- master stylesheet -->
    <link rel="stylesheet" href="{{url('resources/assets/website/css/style.css')}}" type="text/css">
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" href="{{url('resources/assets/website/css/responsive.css')}}" type="text/css">
    <!--Color Switcher Mockup-->
    <!-- <link rel="stylesheet" href="css/color-switcher-design.css"> -->
    <!--Color Themes-->
    <link rel="stylesheet" href="{{url('resources/assets/website/css/color-themes/default-theme.css')}}"  id="theme-color-file" type="text/css">
    <!-- arabic style -->
    <link rel="stylesheet" href="{{url('resources/assets/website/css/arabic/bootstrap.rtl.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('resources/assets/website/css/arabic/rtl.css')}}" type="text/css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('resources/assets/website/images/favicon/apple-touch-icon.png')}}" type="text/css">
    <link rel="icon" type="image/png" href="{{url('resources/assets/website/images/favicon/favicon-32x32.png')}}" sizes="32x32" type="text/css">
    <link rel="icon" type="image/png" href="{{url('resources/assets/website/images/favicon/favicon-16x16.png')}}" sizes="16x16" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
<!--
    <script src="https://maps.googleapis.com/maps/api/js" async defer></script>
-->

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{url('resources/assets/website/js/html5shiv.js')}}"></script>
    <![endif]-->

    <![endif]-->
    @yield('header')
</head>

<body>
<div class="boxed_wrapper">

<!--    <div class="preloader"></div>-->

    <!-- Start Top Bar area -->
    <section class="top-bar-area">
        <div class="container">
            <div class="row">
               
                <div class="col-xl-5 col-lg-6">
                    <div class="top-right">
                       
                        <ul class="sociallinks-style-one">
                             <li><a href="{{$setting[0]->facebook}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="{{$setting[0]->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="{{$setting[0]->linkedin}}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                               <li>
                                    <a href="{{$setting[0]->youtube}}" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                                 <li>
                                    <a href="{{$setting[0]->instagram}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Top Bar area -->

    <!--Start header style1 area-->
    <header class="header-style1-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="inner-content clearfix">
                        <div class="header-style1-logo float-left">
                            <a href="{{url('/')}}">
                                <img src="{{ asset('resources/assets/website/images/resources/logo.png')}}" alt="Awesome Logo">
                            </a>
                        </div>
                        <div class="header-contact-info float-left">
                            <ul>
                                <li>
                                    <div class="single-item">
                                        <div class="icon">
                                            <span class="icon-support"></span>
                                        </div>
                                        <div class="text">
                                            <p>{{$setting[0]->phone}}</p>
                                            <span>{{$setting[0]->email}}</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-item">
                                        <div class="icon">
                                            <span class="icon-gps"></span>
                                        </div>
                                        <div class="text">
                                            <p>{{$setting[0]->address}}</p>
                                            
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="header-style1-button float-right">
                            <a href="{{url('/appointment')}}"><span class="icon-date"></span>حجز الموعد</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--Start mainmenu area-->
    <section class="mainmenu-area stricky">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="inner-content clearfix">
                        <nav class="main-menu style1 clearfix">
                            <div class="navbar-header clearfix">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                
                                    <li  class="{{ (request()->is('/')) ? 'current' : '' }}"><a href="{{url('/')}}">الرئيسية</a>
                                       
                                    
                                    </li>
                                  
                                    <li  class="{{ (request()->is('/about')) ? 'current' : '' }}"> <a href="{{url('/about')}}">من نحن</a></li>

                                    <li  class="{{ (request()->is('/service')) ? 'current' : '' }}" ><a href="{{url('/service')}}">الخدمات</a>
                                       
                                    </li>
                                    <li class="{{ (request()->is('/blog')) ? 'current' : '' }}" ><a href="{{url('/blog')}}">المقالات</a>
                                      
                                    </li>
                                  
                                    <li class="{{ (request()->is('/contact')) ? 'current' : '' }}"><a href="{{url('/contact')}}">اتصل بنا</a></li>
                                </ul>
                            </div>
                        </nav>

                        <div class="mainmenu-right">
<!--
                            <div class="search-box-style1">
                                <form class="search-form" method="post" action="http://steelthemes.com/demo/html/Dento_New/index.html">
                                    <input type="search" name="search" placeholder="Search..." required>
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
-->
<!--
                              <div class="toggler-button">
                                <div class="nav-toggler hidden-bar-opener">
                                    <div class="inner">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>    
                            </div>
-->
                        </div> 
                          
                    </div>
                </div>
            </div>
        </div>
</section>                
<!--End mainmenu area--> 
  
<!-- Hidden Navigation Bar -->
<section class="hidden-bar right-align">
    <div class="hidden-bar-closer">
        <button><span class="flaticon-remove"></span></button>
    </div>
    <div class="hidden-bar-wrapper">
        <div class="logo">
            <a href="index.html"><img src="images/resources/logo-3.png" alt=""/></a>
        </div>
        <div class="contact-info-box">
            <h3>Contact Info</h3>
            <ul>
                <li>
                    <h5>Address</h5>
                    <p>Romanian  9520 Faires Farm Road,<br> Carlsbad, NC 28213.</p>
                </li>
                <li>
                    <h5>Phone</h5>
                    <p>Phone 1: +1 555-7890-123</p>
                </li>
                <li>
                    <h5>Email</h5>
                    <p>supportyou@example.com</p>
                </li>
            </ul>
        </div>       
        <!-- <div class="newsletter-form-box">
            <h3>Newsletter Subscribe</h3>
            <span>Get healthy tips & latest updates in inbox.</span>
            <form action="#">
                <div class="row">
                    <div class="col-xl-12">
                        <input type="email" name="email" placeholder="Email Address..."> 
                        <button type="submit"><span class="flaticon-arrow"></span></button>    
                    </div>
                </div>
            </form>
        </div> -->
        <div class="offer-box text-center">
            <div class="big-title">50% <span>Offer</span></div>
            <h3>5 Years Warranty</h3>
            <a class="btn-one" href="#">Pricing Plans</a>    
        </div>
        <div class="copy-right-text">
            <p>© Do Pave 2019, All Rights Reserved.</p>
        </div> 
    </div>
</section>
<!-- End Hidden Bar --> 

@yield('content')


    <!--Start footer area-->
    <footer class="footer-area">
        <div class="container">
            <div class="row">

                <!--Start single footer widget-->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                    <div class="single-footer-widget marbtm50">
                        <div class="about-us">
                            <div class="footer-logo fix">
                                <a href="index.html">
                                    <img src="{{ asset('resources/assets/website/images/resources/logo-2.png')}}" alt="Awesome Logo">
                                </a>
                            </div>
                            <div class="text-box fix">
                                <p>{!!substr($abouts[0]->description,0,200)!!}</p>
                                <p class="bottom-text">.</p>
                            </div>
                             <div class="button fix">
                                <a class="btn-one"href="{{url('/about')}}">للمزيد</a>
                            </div>    
                        </div>
                    </div>
                </div>
                <!--End single footer widget-->
                    <!--Start single footer widget-->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer-widget martop6 marbtm50">
                    <div class="title">
                        <h3>الخدمات </h3>
                    </div>
                    <ul class="specialities">
                        @foreach($services as $serv) 
                        <li><a href="{{url('servicedetials',$serv->id)}}">{{$serv->name}}</a></li>
                        @endforeach
                       
                    </ul>
                </div>
            </div>
            <!--End single footer widget-->
                  <!--Start single footer widget-->
            <div class="col-xl-4 col-lg-4 col-md-9 col-sm-12">
                <div class="single-footer-widget martop6 pdtop-50">
                    <div class="title">
                        <h3>Opening Hours</h3>
                    </div>
                    <ul class="opening-hours">
                        <li>Monday <span class="float-right">8.30am–6.30pm</span></li>
                        <li>Tuesday <span class="float-right">10.00am–8.00pm</span></li>
                        <li>Wednesday <span class="float-right">8.30am–6.30pm</span></li>
                        <li>Thursday <span class="float-right">8.30am–7.00pm</span></li>
                        <li>Friday <span class="float-right">8.30am–3.00pm</span></li>
                        <li>Saturday <span class="float-right">8.30am–2.00pm</span></li>
                        <li>Sunday <span class="float-right clr-green">Closed</span></li>
                    </ul>   
                </div>
            </div>
            <!--End single footer widget-->

       
            </div>
        </div>
    </footer>
    <!--End footer area-->

    <!--Start footer bottom area-->
    <section class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="inner clearfix">
                        <div class="footer-social-links float-left">
                            <ul class="sociallinks-style-one">
                               <li><a href="{{$setting[0]->facebook}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="{{$setting[0]->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="{{$setting[0]->linkedin}}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                               <li>
                                    <a href="{{$setting[0]->youtube}}" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                                 <li>
                                    <a href="{{$setting[0]->instagram}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="copyright-text text-center">
                            <p>© <a href="https://www.dopave.com/">Do Pave</a> 2019, All Rights Reserved.</p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End footer bottom area-->

</div>

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target thm-bg-clr" data-target="html"><span class="fa fa-angle-up"></span></div>


<!-- /.End Of Color Palate -->
<script src="{{url('resources/assets/website/js/jquery.js')}}"></script>
<!-- Wow Script -->
<script src="{{url('resources/assets/website/js/wow.js')}}"></script>
<!-- bootstrap -->
<script src="{{url('resources/assets/website/js/bootstrap.min.js')}}"></script>
<!-- bx slider -->
<!-- <script src="js/jquery.bxslider.min.js"></script> -->
<!-- count to -->
<script src="{{url('resources/assets/website/js/jquery.countTo.js')}}"></script>
<script src="{{url('resources/assets/website/js/appear.js')}}"></script>
<!-- owl carousel -->
<script src="{{url('resources/assets/website/js/owl.js')}}"></script>
<!-- validate  asset
<script src="{{ asset ('resources/assets/website/js/validation.js')}}"></script>
<-- mixit up -->
<script src="{{url('resources/assets/website/js/jquery.mixitup.min.js')}}"></script>
<!-- isotope script-->
<script src="{{url('resources/assets/website/js/isotope.js')}}"></script>
<!-- Easing -->
<script src="{{url('resources/assets/website/js/jquery.easing.min.js')}}"></script>
<!-- Gmap helper -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyB2uu6KHbLc_y7fyAVA4dpqSVM4w9ZnnUw"></script>
<!--Gmap script-->
<script src="{{url('resources/assets/website/js/gmaps.js')}}"></script>
<script src="{{url('resources/assets/website/js/map-helper.js')}}"></script>
<!-- jQuery ui js -->
<script src="{{url('resources/assets/website/assets/jquery-ui-1.11.4/jquery-ui.js')}}"></script>
<!-- Language Switche  -->
<script src="{{url('resources/assets/website/assets/language-switcher/jquery.polyglot.language.switcher.js')}}"></script>
<!-- jQuery timepicker js -->
<script src="{{url('resources/assets/website/assets/timepicker/timePicker.js')}}"></script>
<!-- Bootstrap select picker js -->
<script src="{{url('resources/assets/website/assets/bootstrap-sl-1.12.1/bootstrap-select.js')}}"></script>

<!-- html5lightbo asset x js -->
<!-- html5lightbox js -->
<script src="{{url('resources/assets/website/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!--Color Switcher-->
<!-- <script src="js/color-settings.js"></script> -->

<!--Revolution Slider-->
<script src="{{url('resources/assets/website/plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{url('resources/assets/website/plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{url('resources/assets/website/js/main-slider-script.js')}}"></script>

<!-- thm custom script -->
<script src="{{url('resources/assets/website/js/custom.js')}}"></script>


<!-- thm custom script -->
<script src="{{url('resources/assets/website/js/custom.js')}}"></script>
<!--
    <script>
        document.getElementsByClassName("hidden-bar-opener").onclick()
    </script>
-->
@yield('footer')
</body>

</html>