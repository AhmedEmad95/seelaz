<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('resources/assets/images/favicon.png')}}">
    <title> Seelaz | @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{url('resources/assets/plugins/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('resources/assets/plugins/css/bootstrap-wysihtml5.css')}}">
    <link rel="stylesheet" href="{{url('resources/assets/plugins/css/select2.min.css')}}">

    <!--This page css - Morris CSS -->
    <link rel="stylesheet" href="{{url('resources/assets/plugins/css/c3.min.css')}}">
    
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{url('resources/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('resources/assets/css/colors/blue.css')}}">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <style>
        .sidebar-nav ul li a
        {
            color: #303030;
        }
    </style>
    <!-- Custom CSS -->

    

    @yield('header')
</head>

<body class="fix-header fix-sidebar card-no-border">

    <div id="main-wrapper">
        <header class="topbar" style="background: #32b6a1;">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{url('/admin')}}">

                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                             <!-- dark Logo text -->
                            <h1 style="color: #ffffff">{{trans('app.comp')}}  </h1>
                        </span>
                    </a>
                            <!-- Light Logo text -->
                </div>
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                      <!--   <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                <ul>
                                    <li>
                                        <div class="drop-title">{{trans('app.notification')}}</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                
                                            </a>
                                            <a href="{{url('admin/shownotification')}}">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
 -->
                      
  

                          <li class="nav-item dropdown">
                            <select  class="selected-language-nav" onchange="myFunction(this.value)">
                                
                                <option <?php
                                        if(Session::get('locale') == 2)
                                            echo "selected";
                                        ?> value="2">English</option>
                                        <option value="1" <?php
                                    if(Session::get('locale') == 1)
                                        echo "selected";
                                    ?> >عربي</option>
                            </select>
                        </li>

                    <!--     <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{url('resources/assets/images/users/1.jpg')}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">


                                    <li><a href="{{route('logout')}}"><i class="fa fa-power-off"></i></a></li>
                                </ul>
                            </div>
                        </li> -->
                  
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                
            <!--    <div class="user-profile" style="background: url ({{url('resources/assets/images/seelaz-logo.png')}}) no-repeat;">
                    <div class="profile-img"> <img src="{{url('resources/assets/images/seelaz-logo.png')}}" alt="user" /> </div>

                    <div class="profile-text"> <a href="#" class="text-center" role="button" aria-haspopup="true" aria-expanded="true">{{Auth::user()->name}}</a>

                    </div>
                </div> -->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="has-arrow waves-effect waves-dark" href="{{url('admin')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">{{trans('app.home')}}</span></a>
                            
                        </li>

                          <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.notification')}} </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/shownotification')}}"> {{trans('app.show')}}</a></li>
                            </ul>
                        </li>

                          <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.account')}} </span></a>
                            <ul aria-expanded="false" class="collapse">
                            <li><a href="{{url('admin/account')}}">Account </a></li>
                                <li><a href="{{url('admin/permission')}}">permission </a></li> 
                                    <li><a href="{{url('admin/role')}}">role</a></li> 
                                
                            </ul>
                        </li>
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.slider')}}</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/adv')}}"> {{trans('app.show')}}</a></li>
                                <li><a href="{{route('adv.create')}}"> {{trans('app.addpaner')}}</a></li>
                            </ul>
                        </li>

                           <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.offer')}} </span></a>
                            <ul aria-expanded="false" class="collapse">
                               
                                
                                <li><a href="{{url('admin/offer')}}"> {{trans('app.offer')}}</a></li>
                                <li><a href="{{route('offer.create')}}"> {{trans('app.addoffer')}}</a></li>
                                
                                
                            </ul>
                        </li>


                           <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.product')}} </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/category')}}">{{trans('app.category')}} </a></li>
                                <li><a href="{{route('category.create')}}"> {{trans('app.addcat')}}</a></li>
                                 <li><a href="{{route('product.create')}}"> {{trans('app.addpro')}}</a></li>
                                <li><a href="{{url('admin/product')}}"> {{trans('app.product2')}}</a></li>
                                 <li><a href="{{url('admin/product1')}}"> {{trans('app.product1')}}</a></li>
                                
                            </ul>
                        </li>
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.blog')}}</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/blog')}}"> {{trans('app.show')}}</a></li>
                                 <li><a href="{{route('blog.create')}}"> {{trans('app.addblog')}}</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.testomanial')}}</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/testomanial')}}"> {{trans('app.show')}}</a></li>
                                 <li><a href="{{route('testomanial.create')}}"> {{trans('app.addtest')}}</a></li>
                            </ul>
                        </li>
                     @permission(('create'))
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">{{trans('app.about')}}</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('admin/about')}}"> {{trans('app.show')}}</a></li>
                            </ul>
                        </li>
                        @endpermission
 
                      
                    </ul>
                </nav>
            </div>
            <br>
            <div class="sidebar-footer">
                <a href="{{route('logout')}}" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i> </a>
            </div>
        </aside>
        <div class="page-wrapper">
            @yield('content')

            <footer class="footer text-center"> ©  Dopave </footer>
        </div>
    </div>
    <script src="{{url('resources/assets/plugins/js/jquery.min.js') }}"></script>

    <script src="{{url('resources/assets/plugins/js/popper.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/bootstrap.min.js') }}"></script>

    <script src="{{url('resources/assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{url('resources/assets/js/waves.js') }}"></script>
    <script src="{{url('resources/assets/js/sidebarmenu.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/sticky-kit.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/jquery.sparkline.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{url('resources/assets/js/custom.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/c3.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/d3.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/select2.full.min.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/jQuery.style.switcher.js') }}"></script>
    <script src="{{url('resources/assets/plugins/js/tinymce.min.js') }}"></script>
    <script>
     function myFunction(val)
        {
            var locale1 = val;
            $.ajax({
                url: "{{url('/language')}}",
                type: "GET",
                dataType: "json",
                data:{locale1:locale1},
                success: function (data)
                {

                },
                error: function (err) {
                    console.log(err);
                },
                complete: function (data) {
                        window.location.reload();
                }
            });
        }
    </script>
    @yield('footer')
</body>

</html>
