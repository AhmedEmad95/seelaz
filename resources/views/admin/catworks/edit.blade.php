@extends('layouts.cpanellayout')
@section('title')
    اضافه قسم جديده
@endsection

@section('header')

@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content">



        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif


    <!-- Main content -->
        <section class="content">
            <!-- left column -->
            <div class="col-md-12">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Forms</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/catwork')}}">تصنيفات الاعمال السابقة </a> </li>
                            <li class="breadcrumb-item active">اضافة </li>
                        </ol>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" role="form" action="{{route('catworks.update',$category->id)}}"  accept-charset="utf-8" enctype="multipart/form-data"  >
                    {{method_field('PATCH')}}
                        {{csrf_field()}}
                        
                        <div class="card card-body col-md-6">
                            <div class="form-group">
                                <label class="control-label">اسم التصنف<span style="color:red;">*</span>:</label>
                                <div class="controls">
                                  <input type="text" name="name" id="category_name" class="form-control" id="name" placeholder=" اسم التصنف" value="{{$category->name}}" required >
                                </div>
                            </div>
                            
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">Edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    

@endsection