@extends('layouts.cpanellayout')
@section('title')
    {{trans('app.edit')}}
@endsection

@section('header')
  <style>
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
    </style>

@endsection

@section('content')
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close pull-left" data-dismiss="alert">x</button>
                <strong>خطاء!</strong> رجاء ادخال بيانات صحيحه<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <section class="content">
            <div class="box box-body">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">{{trans('app.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/category')}}">{{trans('app.blog')}} </a> </li>
                            <li class="breadcrumb-item active">{{trans('app.edit')}}  </li>
                        </ol>
                    </div>
                </div>
                <div class="box box-primary">
                    <form action="{{route('blog.update',$blog[0]->ids)}}" method="post" role="form" accept-charset="utf-8" enctype="multipart/form-data">
                        {{method_field('PATCH')}}
                        {{csrf_field()}}
                        <div class="card card-body col-md-12">
                            <div class="form-group">
                                <label for="name">{{trans('app.title')}}<span style="color:red;">*</span>:</label>
                                <input name="title" value="{{$blog[0]->title}}" type="text" class="form-control" id="name" required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.englishtitle')}}<span style="color:red;">*</span>:</label>
                                <input name="englishtitle" value="{{$blog[1]->title}}" type="text" class="form-control" id="name" required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.descreption')}}<span style="color:red;">*</span>:</label>
                                <textarea name="descreption" value="" class="form-control" rows="10" id="descreption"> {{$blog[0]->descreption}} </textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.englishdescreption')}}<span style="color:red;">*</span>:</label>
                                <textarea name="englishdescreption" class="form-control" rows="10" id="productdescreptionenglish"> {{$blog[1]->descreption}} </textarea>
                            </div>
                             <div class="form-group">
                                <label for="exampleInputFile">{{trans('app.image')}} </label>
                                <input type="file" name="image"  id="files" class="form-control"   enctype="multipart/form-data"  >
                            </div>
                            <div class="form-group">
                                <label for="name"> {{trans('app.image')}} <span style="color:red;">*</span>:</label>
                                <a href="{{url('storage/app/'.$blog[0]->image)}}" onclick="window.open(this.href, '_blank', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;">
                                    <img src="{{url('storage/app/'.$blog[0]->image)}}" style="width: 150px; height: 90px;" alt="homepage" class="light-logo" />
                                </a>
                            </div>
                             <div class="form-group">
                                <label for="name"> {{trans('app.alt')}} <span style="color:red;">*</span>:</label>
                                <input type="text" name="alt"  class="form-control" value="{{$blog[0]->alt}}" >
                            </div>
                             <div class="form-group">
                                <label for="name">{{trans('app.des')}}  <span style="color:red;">*</span>:</label>
                                <input type="text" name="des" value="{{$blog[0]->des}}"  class="form-control"  required>
                            </div>
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">{{trans('app.edit')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.row -->
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('descreption');
        CKEDITOR.replace('productdescreptionenglish');


              $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            console.log(i);
                            $("<span class=\"pip\">" +
                                "<img  class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"dvdf" + file.name + "\"/>" +
                                    {{--"<br/><label for=\"name\">   {{trans('app.alt')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"alt"+i+"\"/>" +--}}
                                            {{--"<br/><label for=\"name\">   {{trans('app.des')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"des"+i+"\"/>" +--}}
                                        "<span class=\"remove\">Remove image</span>" +
                                "</span>").insertAfter("#files");
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                                $('#files').val("");
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
        </script>
        @endsection