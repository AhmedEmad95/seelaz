@extends('layouts.cpanellayout')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Advertisment
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('ads.index')}}">Ads</a></li>
            <li class="active">Edit Ads</li>
        </ol>
    </section>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (\Session::has('warning'))
    <div class="col-xs-12">
        <div class="alert alert-warning">
            <strong> {{ session()->get('warning') }}</strong>
        </div>
    </div>
    @endif

    @if (\Session::has('success'))
    <div class="col-xs-12">
        <div class="alert alert-success">
            <strong>success</strong> {{ session()->get('success') }}
        </div>
    </div>
    @endif
    @if (\Session::has('no'))
    <div class="col-xs-12">
        <div class="alert alert-success">
            <strong>no </strong>
        </div>
    </div>
    @endif



    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Advertisment</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('ads.update',$Adtoedit->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field () }}
                        {{method_field('PUT')}}
                        <div class="box-body">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Url</label>
                              <input  type="text" required class="form-control" name="url" value="{{$Adtoedit->url}}" id="exampleInputEmail1" placeholder=" url">
                          </div>

                          <div class="form-group">
                              <label for="exampleInputFile">image</label>
                              <input type="file" name="image" id="exampleInputFile">

                          </div>

                      </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
