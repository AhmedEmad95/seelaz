@extends('layouts.cpanellayout')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
ارسل تنبيه
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (\Session::has('warning'))
    <div class="col-xs-12">
        <div class="alert alert-warning">
            <strong> {{ session()->get('warning') }}</strong>
        </div>
    </div>
    @endif

    @if (\Session::has('success'))
    <div class="col-xs-12">
        <div class="alert alert-success">
            <strong>success</strong>{{ session()->get('success') }}
        </div>
    </div>
    @endif
    @if (\Session::has('no'))
    <div class="col-xs-12">
        <div class="alert alert-success">
            <strong>no </strong>
        </div>
    </div>
    @endif


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12" dir="rtl">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> تفاصيل التنبيه</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{url('admin/sendnotification')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field () }}
                        <div class="box-body">

                          <div class="form-group">
                            <label for="exampleInputEmail1">محتوى التنبيه</label>
                            <textarea required class="form-control" id="description" name="notificationcontent">{{ old('description') }}</textarea>
                        </div>

                          <div class="form-group">
                            <label>اختر نوع مستلمى التنبيه</label>
                            <select class="form-control select2" required  name="user_category" style="width: 100%;">
                              <option selected="selected" disabled="disabled">اختر هنا </option>
                              <option value="provider">الموردين</option>
                              <option value="shopkeper">اصاحبى المحلات </option>
                              <option value="0">الكل </option>
                            </select>
                          </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">ارسل</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
