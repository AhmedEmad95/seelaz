@extends('layouts.cpanellayout')
@section('title')
    {{trans('app.edit')}}
@endsection

@section('header')

@endsection

@section('content')
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close pull-left" data-dismiss="alert">x</button>
                <strong>خطاء!</strong> رجاء ادخال بيانات صحيحه<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <section class="content">
            <div class="box box-body">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">{{trans('app.home')}}</a></li>
                            <li class="breadcrumb-item active">{{trans('app.about')}}</li>
                            <li class="breadcrumb-item active">{{trans('app.edit')}}  </li>
                        </ol>
                    </div>
                </div>
                <div class="box box-primary">
                    <form action="{{route('about.update',$about[0]->id)}}" method="post" role="form" accept-charset="utf-8" enctype="multipart/form-data">
                        {{method_field('PATCH')}}
                        {{csrf_field()}}
                        <div class="card card-body col-md-12">
                            <div class="form-group">
                                <label for="name">{{trans('app.aboutcompany')}}<span style="color:red;">*</span>:</label>
                                <input name="about" type="text" class="form-control" value="{{$about[0]->about}}" id="name" placeholder="{{trans('app.productname')}} " required />
                            </div>

                            <div class="form-group">
                                <label for="name">{{trans('app.aboutenglish')}}<span style="color:red;">*</span>:</label>
                                <input name="aboutenglish" type="text" value="{{$about[1]->about}}" class="form-control"  id="name" placeholder="{{trans('app.productnameenglish')}} " required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.descreption')}}<span style="color:red;">*</span>:</label>
                                <textarea name="descreption" value="" class="form-control" rows="10" id="descreption"> {{$about[0]->whyus}} </textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.englishdescreption')}}<span style="color:red;">*</span>:</label>
                                <textarea name="englishdescreption" class="form-control" rows="10" id="productdescreptionenglish"> {{$about[1]->whyus}} </textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.telephone')}}<span style="color:red;">*</span>:</label>
                                <input name="telephone" type="text" value="{{$about[0]->telephone}}" class="form-control"  id="name"  required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.address')}}<span style="color:red;">*</span>:</label>

                                <textarea name="address" value="" class="form-control" rows="10" id="descreption"> {{$about[0]->address}} </textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.email')}}<span style="color:red;">*</span>:</label>
                                <input name="email" type="text" value="{{$about[0]->email}}" class="form-control"  id="name"  required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.facebook')}}<span style="color:red;">*</span>:</label>
                                <input name="facebook" type="text" value="{{$about[0]->facebook}}" class="form-control"  id="name"  required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.twitter')}}<span style="color:red;">*</span>:</label>
                                <input name="twitter" type="text" value="{{$about[0]->twitter}}" class="form-control"  id="name"  required />
                            </div>
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">{{trans('app.edit')}} </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.row -->
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('descreption');
        CKEDITOR.replace('productdescreptionenglish');


              $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            console.log(i);
                            $("<span class=\"pip\">" +
                                "<img  class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"dvdf" + file.name + "\"/>" +
                                    {{--"<br/><label for=\"name\">   {{trans('app.alt')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"alt"+i+"\"/>" +--}}
                                            {{--"<br/><label for=\"name\">   {{trans('app.des')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"des"+i+"\"/>" +--}}
                                        "<span class=\"remove\">Remove image</span>" +
                                "</span>").insertAfter("#files");
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                                $('#files').val("");
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
        </script>
        @endsection