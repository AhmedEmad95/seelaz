@extends('layouts.cpanellayout')
@section('title')
    {{trans('app.edit')}} {{trans('app.product')}}
@endsection

@section('header')
    <style>
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
    </style>
@endsection

@section('content')
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close pull-left" data-dismiss="alert">x</button>
                <strong>خطاء!</strong> رجاء ادخال بيانات صحيحه<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <section class="content">
            <div class="box box-body">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">{{trans('app.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/product')}}">{{trans('app.product')}} </a> </li>
                            <li class="breadcrumb-item active">{{trans('app.edit')}} </li>
                        </ol>
                    </div>
                </div>
                <div class="box box-primary">
                    <form action="{{route('product.update',$product[0]->id)}}" method="post" role="form" accept-charset="utf-8" enctype="multipart/form-data">
                        {{method_field('PATCH')}}
                        {{csrf_field()}}
                        <div class="card card-body col-md-12">
                            <div class="form-group">
                                <label for="name">{{trans('app.productname')}}<span style="color:red;">*</span>:</label>
                                <input name="name" type="text" class="form-control" value="{{$product[0]->name}}" id="name" placeholder="{{trans('app.productname')}} " required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.productnameenglish')}}<span style="color:red;">*</span>:</label>
                                <input name="productnameenglish" type="text" value="{{$product[1]->name}}" class="form-control"  id="name" placeholder="{{trans('app.productnameenglish')}} " required />
                            </div>
                            <div class="form-group">
                                <label for="name"> {{trans('app.productdescreption')}} <span style="color:red;">*</span>:</label>
                                <textarea name="descreption" type="text" class="form-control" id="descreption">
                                    {{$product[0]->descreption}}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.productdescreptionenglish')}} <span style="color:red;">*</span>:</label><textarea name="productdescreptionenglish" type="text" class="form-control" id="productdescreptionenglish">
                                    {{$product[1]->descreption}}
                                </textarea>
                            </div>

                              <div class="form-group">
                                <label for="name">price before<span style="color:red;">*</span>:</label>
                                <input name="p_price" type="text" class="form-control"  value="{{$product[0]->p_price}}" id="name" placeholder="price Before "  required />
                            </div>
                             <div class="form-group">
                                <label for="name">price After<span style="color:red;">*</span>:</label>
                                <input name="c_price" type="text" class="form-control"  value="{{$product[0]->c_price}}" id="name" placeholder="price After "  required />
                            </div>
                              <div class="form-group">
                                <label for="name"> {{trans('app.productdiscount')}} <span style="color:red;">% *</span>:</label>
                                <input name="offer" type="number" class="form-control"  value="{{$product[0]->offer}}" id="name" placeholder="{{trans('app.productdiscount')}}" required />
                            </div>
                          
                            <div class="form-group">
                                <select class="form-control" name="categore_id"  onchange="SubcategoryFunction(this.value)">
                                    <option id="cat" disabled selected>{{trans('app.cat')}} </option>
                                    @for($i=0;$i<count($category);$i++)
                                        <option id="cat" name="cat" value="{{$category[$i]->categore_id}}"
                                        <?php
                                            if ($category[$i]->categore_id == $product[0]->categore_id)
                                                echo "selected";
                                        ?>
                                        >{{$category[$i]->name}}</option>
                                    @endfor
                                </select>

                            </div>


                                <div class="form-group">
                                <label for="name"> {{trans('app.alt')}} <span style="color:red;">*</span>:</label>
                                <input type="text" name="alt" value="{{$product[0]->alt}}"   class="form-control"  >
                            </div>

                            
                     
                            <div class="form-group">
                                <label for="exampleInputFile">{{trans('app.productimage')}}</label>
                                <input type="file"  name="image[]" multiple id="files" class="form-control"  enctype="multipart/form-data">
                            </div>

                             
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">{{trans('app.edit')}}</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.row -->
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('descreption');
        CKEDITOR.replace('productdescreptionenglish');

        $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            console.log(i);
                            $("<span class=\"pip\">" +
                                "<img  class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"dvdf" + file.name + "\"/>" +
                                    {{--"<br/><label for=\"name\">   {{trans('app.alt')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"alt"+i+"\"/>" +--}}
                                            {{--"<br/><label for=\"name\">   {{trans('app.des')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"des"+i+"\"/>" +--}}
                                        "<span class=\"remove\">Remove image</span>" +
                                "</span>").insertAfter("#files");
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                                $('#files').val("");
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
    </script>

@endsection