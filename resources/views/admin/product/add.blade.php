@extends('layouts.cpanellayout')
@section('title')
   {{trans('app.add')}} {{trans('app.product')}}
@endsection

@section('header')
    <style>
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
    </style>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content">



        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif


    <!-- Main content -->
        <section class="content">
            <!-- left column -->
            <div class="col-md-12">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">{{trans('app.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/product')}}">{{trans('app.product')}} </a> </li>
                            <li class="breadcrumb-item active">{{trans('app.add')}} </li>
                        </ol>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{route('product.store')}}" method="post" role="form" accept-charset="utf-8" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="card card-body col-md-12">
                            <div class="form-group">
                                <label for="name">{{trans('app.productname')}}<span style="color:red;">*</span>:</label>
                                <input name="name" type="text" class="form-control" id="name" placeholder="{{trans('app.productname')}} " required />
                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.productnameenglish')}}<span style="color:red;">*</span>:</label>
                                <input name="productnameenglish" type="text" class="form-control" id="name" placeholder=" {{trans('app.productnameenglish')}} " required />

                            </div>
                            <div class="form-group">
                                <label for="name"> {{trans('app.productdescreption')}} <span style="color:red;">*</span>:</label>
                                <textarea name="descreption" type="text" class="form-control" id="descreption">

                                </textarea>                            </div>
                            <div class="form-group">
                                <label for="name">{{trans('app.productdescreptionenglish')}} <span style="color:red;">*</span>:</label>
                                <textarea name="productdescreptionenglish" type="text" class="form-control" id="productdescreptionenglish">

                                </textarea>
                            </div>
                            <div class="form-group ">
                                <label for="name">{{trans('app.cat')}} <span style="color:red;">*</span>:</label>
                                <select class="form-control" name="categore_id" required>
                                    <option id="cat" value="">{{trans('app.cat')}} </option>
                                    @for($i=0;$i<count($category);$i++)
                                        <option id="cat" name="cat" value="{{$category[$i]->categore_id}}">{{$category[$i]->name}}</option>
                                    @endfor
                                </select>
                            </div>
                             <div class="form-group ">
                                <label for="name">الفئة المستهدفة  <span style="color:red;">*</span>:</label>
                                <select class="form-control" name="type"  required>
                                    <option id="cat" value="">الفئة المستهدفة </option>
                                    
                                        <option id="cat" name="cat" value="0">provider</option>
                                        <option id="cat" name="cat" value="1">customer</option>
                                  
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">{{trans('app.productimage')}}</label>
                                <input type="file"   name="image[]"  id="files" multiple class="form-control"  enctype="multipart/form-data"   required>
                            </div>
                               <div class="form-group">
                                <label for="name"> {{trans('app.alt')}} <span style="color:red;">*</span>:</label>
                                <input type="text" name="alt"  class="form-control"  required>
                            </div>
                             <div class="form-group">
                                <label for="name">{{trans('app.des')}}  <span style="color:red;">*</span>:</label>
                                <input type="text" name="des"  class="form-control"  required>
                            </div>

                             <div class="form-group">
                                <label for="name">price Before<span style="color:red;">*</span>:</label>
                                <input name="p_price" type="text" class="form-control" id="name" placeholder="price Before " required />
                            </div>
                             <div class="form-group">
                                <label for="name">price After<span style="color:red;">*</span>:</label>
                                <input name="c_price" type="text" class="form-control" id="name" placeholder="price After" required />
                            </div>

                            <div class="form-group">
                                <label for="name"> {{trans('app.productdiscount')}} <span style="color:red;">% *</span>:</label>
                                <input name="offer" type="number" class="form-control" id="name" placeholder="{{trans('app.productdiscount')}}" required />
                            </div>
                           
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">اضافه</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
@section('footer')
    <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('descreption');
        CKEDITOR.replace('productdescreptionenglish');

        $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            console.log(i);
                            $("<span class=\"pip\">" +
                                "<img  class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"dvdf" + file.name + "\"/>" +
                                    {{--"<br/><label for=\"name\">   {{trans('app.alt')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"alt"+i+"\"/>" +--}}
                                            {{--"<br/><label for=\"name\">   {{trans('app.des')}}  <span style=\"color:red;\">*</span>:</label>\n<input  class=\"\"   +  name==\"des"+i+"\"/>" +--}}
                                        "<span class=\"remove\">Remove image</span>" +
                                "</span>").insertAfter("#files");
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                                $('#files').val("");
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
    </script>

@endsection