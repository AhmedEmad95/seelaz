@extends('layouts.cpanellayout')
@section('title')
    اضافه قسم جديده
@endsection

@section('header')

@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content">



        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif


    <!-- Main content -->
        <section class="content">
            <!-- left column -->
            <div class="col-md-12">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Forms</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/testimonial')}}">التوصيات </a> </li>
                            <li class="breadcrumb-item active">تعديل </li>
                        </ol>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{route('testimonial.update',$testimonial->id)}}" method="post" role="form" accept-charset="utf-8" enctype="multipart/form-data"  >
                    {{method_field('PATCH')}}
                        {{csrf_field()}}
                        <div class="card card-body col-md-12">
                        <div class="form-group">
                                <label class="control-label">الاسم<span style="color:red;">*</span>:</label>
                                <div class="controls">
                                  <input type="text" name="name" value="{{$testimonial->name}}" id="name" class="form-control" id="name" placeholder=" الاسم " required >
                                </div>
                                </div>
                                 <div class="form-group">
                                <label class="control-label">المدنية<span style="color:red;">*</span>:</label>
                                <div class="controls">
                                  <input type="text" name="city" value="{{$testimonial->city}}" id="name" class="form-control" id="name" placeholder=" المدينة " required >
                                </div>
                                </div>



                        <div class="form-group">
                                <label for="exampleInputFile"> الصورة</label>
                                <input type="file"  name="image" multiple class="form-control"  enctype="multipart/form-data"  >
                             @if(!empty($testimonial->image))
                            <input type="hidden" name="current_image" value="{{ $testimonial->image }}"> 
                           <img style="width: 120px" src="{{ asset('resources/assets/images/backend_images/testimonial/large/'.$testimonial->image) }}">| 
                              @endif 
                            </div>
                          
                            <div class="form-group">
                                <label for="name"> الوصف <span style="color:red;">*</span>:</label>
                                <textarea name="description" type="text" class="form-control" id="descreption">
                                    {{$testimonial->description}}

                                </textarea>
                                </div>
                                 
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pu">تعديل</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

     <script src="{{url('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>


   <script>
       CKEDITOR.replace('descreption');
       </script>


    

@endsection