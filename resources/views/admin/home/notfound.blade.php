@extends('layouts.websitlayout')

@section('title')
    {{trans('app.error')}}

@endsection
@section('header')
    <style>
        .text {
            text-transform: uppercase;
            font-family: verdana;
            font-size: 12em;
            font-weight: 700;
            color: #f5f5f5;
            text-shadow: 1px 1px 1px #919191,
            1px 2px 1px #919191,
            1px 3px 1px #919191,
            1px 4px 1px #919191,
            1px 5px 1px #919191,
            1px 6px 1px #919191,
            1px 7px 1px #919191,
            1px 8px 1px #919191,
            1px 9px 1px #919191,
            1px 10px 1px #919191,
            1px 18px 6px rgba(16,16,16,0.4),
            1px 22px 10px rgba(16,16,16,0.2),
            1px 25px 35px rgba(16,16,16,0.2),
            1px 30px 60px rgba(16,16,16,0.4);
        }
        .text-2{
            text-transform: capitalize;
            font-size: 30px;
        }

        .ERROR-link {

            text-transform: capitalize;
        }
        .ERROR-link:hover{
            color: #000;
        }
    </style>
@endsection
@section('content')
    <section class="text-center pt-t">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text" >404</h1>
                    <p class="text-2">    {{trans('app.error')}}
                    </p>
                </div>
                <div class="col-lg-12">
                    <a class="ERROR-link" href="{{url('/')}}">    {{trans('app.home')}}
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')

@endsection
