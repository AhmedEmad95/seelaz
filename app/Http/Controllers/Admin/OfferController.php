<?php

namespace App\Http\Controllers\Admin;

use App\Offer;
use App\OfferLanguage;
use App\OfferProduct;
use App\Product;
use App\ProductImage;
use App\ProductLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use File;
use Session;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $offer=array();
        $lang=Session::get('locale');
          $offer = OfferProduct::
       join('offers', 'offers.id','offer_products.offer_id')->
       join('products', 'products.id','offer_products.product_id')->
       join('offer_languages', 'offer_languages.offer_id', '=', 'offers.id')->
       join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->select('offers.*','offer_languages.name','offer_languages.descreption','offers.image as image' ,'product_languages.name as product_name')
            ->where('offer_languages.language_id',$lang)
            ->where('product_languages.language_id',$lang)
            ->get();
        //dd( $offer);

        return view('admin.offer.index',compact('offer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $lang=Session::get('locale');
        $product = DB::table('products')
            ->join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->select('products.id as ids','product_languages.*')
            ->where('product_languages.language_id',$lang)
            ->get();
           // dd($product);
        return view('admin.offer.add',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $product=$request->product;
          $offer=new Offer();
          $offer->alt=$request->input('alt');
     
        $file = $request->image;
        if ($request->hasFile('image')) {
            $extension = $file->getClientOriginalExtension();
            $name = sha1($file->getClientOriginalName());
            $imgname = date('y-m-d') . $name . "." . $extension;
            $path = storage_path('app/offer/');
            $file->move($path, $imgname);
            $offer->image = 'offer'.'/'.$imgname;
        }

        $offer->save();
        $id=$offer->id;
        $offerlang=new offerLanguage();
        $offerlang->name=$request->name;
        $offerlang->descreption=$request->descreption;
        $offerlang->language_id=1;
        $offerlang->offer_id=$id;
        $offerlang->save();
         $offerlang=new offerLanguage();
        $offerlang->name=$request->offernameenglish;
         $offerlang->descreption=$request->offerdescreptionenglish;
         $offerlang->language_id=2;
        $offerlang->offer_id=$id;
         $offerlang->save();

         /* $offerpro= new OfferProduct();
            $offerpro->offer_id=$id;
              $offerpro->product_id=$request->input('product_id');
              $offerpro->save();
*/    // dd($product);
         for ($i=0;$i<count($product);$i++)
        {
            $offerpro=new OfferProduct();
            $offerpro->offer_id=$id;
            $offerpro->product_id=$product[$i];
            $offerpro->save();
           
        }

        return redirect('/admin/offer')->with('success', 'Add blog successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lang=Session::get('locale');
         $offer = DB::table('offers')
            ->join('offer_languages', 'offer_languages.offer_id', '=', 'offers.id')
            ->select('offers.id as ids','offer_languages.*','offers.image as image','offers.alt as alt')
            ->where('offer_languages.offer_id',$id)
            ->get();
            $product = DB::table('products')
            ->join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->select('products.id as ids','product_languages.*')
            ->where('product_languages.language_id',$lang)
            ->get();

                //dd($offer);
          // dd( $blog);
             return view('admin.offer.edit',compact('offer','product',' $offer_products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           
        $offer=Offer::find($id);
      $offer->alt=$request->input('alt');
     
        $file = $request->image;
        if ($request->hasFile('image'))
        {
             $image_path = "storage/app/" . $offer['image'];
            //file path
                if (File::exists( $image_path)) {
                    File::delete( $image_path);
                }
            $extension = $file->getClientOriginalExtension();
            $name = sha1($file->getClientOriginalName());
            $imgname = date('y-m-d') . $name . "." . $extension;
            $path = storage_path('app/offer/');
            $file->move($path, $imgname);
            $offer->image = 'offer'.'/'.$imgname;
        }

        $offer->save();
        $offer_id=$offer->id;
        $blog_lang=OfferLanguage::where('offer_id',$offer_id)->get();
        $blog_arabic=OfferLanguage::find($blog_lang[0]['id']);
        $blog_arabic->name=$request->name;
        $blog_arabic->descreption=$request->descreption;
        $blog_lang->offer_id=$id;
        $blog_arabic->save();
        $blog_english=OfferLanguage::find($blog_lang[1]['id']);
        $blog_english->name=$request->offernameenglish;
        $blog_english->descreption=$request->offerdescreptionenglish;
        $blog_lang->offer_id=$id;
        $blog_english->save();
        $product=$request->product;
           for ($i=0;$i<count($product);$i++)
        {
            $offerpro=new OfferProduct();
            $offerpro->offer_id=$id;
            $offerpro->product_id=$product[$i];
            $offerpro->save();
           
        }
     
        return redirect('/admin/offer')->with('update', 'category Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);
        $image_path = "storage/app/" . $offer['image'];  // Value is not URL but directory file path
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        
        $offer->destroy($id);
        return redirect('/admin/offer')->with('delete', 'product deleted successfully');
    }
}
