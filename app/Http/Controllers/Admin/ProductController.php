<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Product;
use App\ProductImage;
use App\ProductLanguage;
use App\SubCategory;
use App\SubCategoryLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use File;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=array();
        $lang=Session::get('locale');
        $products =Product::
            join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->join('category_languages', 'category_languages.categore_id', '=', 'products.categore_id')
            ->select('products.*','product_languages.name','product_languages.descreption','category_languages.name as category_name')
            ->where('category_languages.language_id',$lang)
            ->where('product_languages.language_id',$lang)
            ->where('products.type','=','1')
            ->get();

              for ($i=0;$i<count($products);$i++)
        {
           $images=ProductImage::select('image')->where('product_id',$products[$i]->id)->first();
            $products[$i]['id']=$products[$i]->id;
            $products[$i]['category_name']=$products[$i]->category_name;
            $products[$i]['name']=$products[$i]->name;
            $products[$i]['descreption']=$products[$i]->descreption;
            $products[$i]['created_at']=$products[$i]->created_at;
            $products[$i]['image']=$images['image'];
        }
         

  // dd($products);
        return view('admin.product.index',compact('products'));
    }

        public function index1()
    {
        $products=array();
        $lang=Session::get('locale');
        $products =Product::
            join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->join('category_languages', 'category_languages.categore_id', '=', 'products.categore_id')
            ->select('products.*','product_languages.name','product_languages.descreption','category_languages.name as category_name')
            ->where('category_languages.language_id',$lang)
            ->where('product_languages.language_id',$lang)
            ->where('products.type','=','0')
            ->get();


              for ($i=0;$i<count($products);$i++)
        {
            $images=ProductImage::select('image')->where('product_id',$products[$i]->id)->first();
            $products[$i]['id']=$products[$i]->id;
            $products[$i]['category_name']=$products[$i]->category_name;
            $products[$i]['name']=$products[$i]->name;
            $products[$i]['descreption']=$products[$i]->descreption;
            $products[$i]['created_at']=$products[$i]->created_at;
            $products[$i]['image']=$images['image'];
        }
    // dd($products);
        return view('admin.product.index1',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang=Session::get('locale');
        $category = DB::table('categories')
            ->join('category_languages', 'category_languages.categore_id', '=', 'categories.id')
            ->select('categories.id as ids','category_languages.*')
            ->where('category_languages.language_id',$lang)
            ->get();
        return view('admin.product.add',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $product=new Product();
        $product->categore_id=$request->input('categore_id');
        $product->p_price=$request->input('p_price');
         $product->c_price=$request->input('c_price');
         $product->alt=$request->input('alt');
        $product->des=$request->input('des');
       
         $product->type=$request->input('type');
          $product->offer=$request->input('offer');
    
        $product->save();
        $product_id=$product->id;
        $productlang=new ProductLanguage();
        $productlang->product_id=$product_id;
        $productlang->name=$request->name;
        $productlang->language_id=1;
        $productlang->descreption=$request->descreption;
        $productlang->save();
        $productlang=new ProductLanguage();
        $productlang->product_id=$product_id;
        $productlang->name=$request->productnameenglish;
        $productlang->language_id=2;
        $productlang->descreption=$request->productdescreptionenglish;
        $productlang->save();
        $product_id=$product->id;
        $allimages = $request->image;
        if ($request->hasFile('image')) {
            foreach ($allimages as $file) {
                $img=new ProductImage();
                $extension = $file->getClientOriginalExtension();
                $name = sha1($file->getClientOriginalName());
                $imgname = date('y-m-d') . $name . "." . $extension;
                $path = storage_path('app/product/');
                $file->move($path, $imgname);
                $img->image = 'product'.'/'.$imgname;
                $img->product_id=$product_id;
                $img->save();
            }
           //dd(   $allimages);

        }
        return redirect('/admin/product')->with('add', 'Add Adv successfully');   
      
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lang=Session::get('locale');
        $product = DB::table('products')
            ->join('product_languages', 'product_languages.product_id', '=', 'products.id')
            ->join('product_images', 'product_images.product_id', '=', 'products.id')
            ->select('products.*','product_languages.name','product_languages.descreption','product_images.image as image')
            ->where('products.id',$id)
            ->where('product_languages.language_id',$lang)
            ->get();
           // dd($product);
        $category = DB::table('categories')
            ->join('category_languages', 'category_languages.categore_id', '=', 'categories.id')
            ->select('category_languages.*')
            ->where('category_languages.language_id',$lang)
            ->get();
           // dd($product);

             // $image = ProductImage::where('product_id', $id)->get();
          
         //dd(  $image);



        return view('admin.product.edit',compact('product','category','image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    //    return $request->all();
        $product=Product::find($id);
        $product->categore_id=$request->input('categore_id');
          $product->p_price=$request->input('p_price');
        $product->c_price=$request->input('c_price');
         $product->offer=$request->input('offer');
        $product->save();
        $product_id= $product->id;
        $pr_lang=ProductLanguage::where('product_id',$product_id)->get();
        $productlanguage=ProductLanguage::find($pr_lang[0]['id']);
        $productlanguage->name=$request->name;
        if (isset($request->descreption)) {
            $productlanguage->descreption=$request->descreption;
        }
        $productlanguage->save();
        $productlanguage1=ProductLanguage::find($pr_lang[1]['id']);
        $productlanguage1->name=$request->productnameenglish;
        $productlanguage1->descreption=$request->productdescreptionenglish;
        $productlanguage1->save();

           $allimages = $request->image;
        if ($request->hasFile('image')) {
            $image = ProductImage::where('product_id', $id)->get();
            for ($i = 0; $i < count($image); $i++) {
                $image_path = "storage/app/" . $image[$i]['image'];  // Value is not URL but directory file path
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                $im=ProductImage::find($image[$i]['id']);
                $im->destroy($image[$i]['id']);
            }
            foreach ($allimages as $file) {
                $img=new ProductImage();
                $extension = $file->getClientOriginalExtension();
                $name = sha1($file->getClientOriginalName());
                $imgname = date('y-m-d') . $name . "." . $extension;
                $path = storage_path('app/product/');
                $file->move($path, $imgname);
                $img->image = 'product'.'/'.$imgname;
                $img->product_id=$product_id;
                $img->save();
            }
           // dd(  $image);
        }
     
        return redirect('/admin/product')->with('update', 'category Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $image = ProductImage::where('product_id', $id)->get();
        for ($i = 0; $i < count($image); $i++) {
            $image_path = "storage/app/" . $image[$i]['image'];  // Value is not URL but directory file path
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
        }
        $product->destroy($id);
        return redirect('/admin/product')->with('delete', 'product deleted successfully');
    }
    public function pindingproduct($id)
    {
        $product = Product::find($id);
        $product->pinding=0;
        $product->save();
        return redirect('/admin/product')->with('pinding', 'product pinding successfully');
    }
    public function activeproduct($id)
    {
        $product = Product::find($id);
        $product->pinding=1;
        $product->save();
        return redirect('/admin/product')->with('active', 'product pinding successfully');
    }
}
