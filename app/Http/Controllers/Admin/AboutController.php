<?php

namespace App\Http\Controllers\admin;

use App\About;
use App\AboutLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use File;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = Session::get('locale');
        $about = DB::table('abouts')
            ->join('about_languages', 'about_languages.about_id', '=', 'abouts.id')
            ->select('abouts.*', 'about_languages.about','about_languages.whyus')
            ->where('about_languages.language_id', $lang)
            ->get();
            //dd( $about);

        return view('admin.about.index', compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = DB::table('abouts')
            ->join('about_languages', 'about_languages.about_id', '=', 'abouts.id')
            ->select('abouts.*','about_languages.about', 'about_languages.whyus')
            ->where('abouts.id',$id)
            ->get();
           //dd($about);

        return view('admin.about.edit',compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //    return $request->all();
        $about=About::find($id);
        $about->telephone=$request->telephone;
        $about->email=$request->email;
        $about->facebook=$request->facebook;
        $about->twitter=$request->twitter;
        $about->address=$request->address;
        $about->save();
        $about_id= $about->id;
        $about_language=AboutLanguage::where('about_id',$about_id)->get();
        $about_lan=AboutLanguage::find($about_language[0]['id']);
        $about_lan->about=$request->about;
        $about_lan->whyus=$request->descreption;
        $about_lan->save();
        $about_lan1=AboutLanguage::find($about_language[1]['id']);
        $about_lan1->about=$request->aboutenglish;
        $about_lan->whyus=$request->englishdescreption;
        $about_lan1->save();
        return redirect('/admin/about')->with('update', 'about Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
