<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Product;
use Illuminate\Http\Request;
use App\User;
use App\Message;
use Carbon;
use Illuminate\Support\Facades\Validator;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         return view('admin.home.home');
    }


     public function Changelang(Request $request)
    {
        $language= $request->locale1;
        Session::put('locale', $language);
        return response()->json(['Status' => 'success','message'=>$language],200);
    }
}
